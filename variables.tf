variable "name" {
  default     = "the_laziest_of_names"
  description = "enter a name for the autoscaling resources to tie them together"
}
