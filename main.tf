resource "aws_placement_group" "autoscaling" {
  name     = var.name
  strategy = "spread"
}

resource "aws_autoscaling_group" "main_group" {
  name                      = var.name
  max_size                  = var.max_size
  min_size                  = var.min_size
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = "ELB"
  desired_capacity          = var.desired_capacity
  force_delete              = true
  placement_group           = "${aws_placement_group.autoscaling.id}"
  launch_configuration      = var.launch_configuration
  vpc_zone_identifier       = var.subnets


}

resource "aws_autoscaling_lifecycle_hook" "default" {
  name                   = var.name
  autoscaling_group_name = "${aws_autoscaling_group.main_group.name}"
  default_result         = "ABANDON"
  heartbeat_timeout      = var.heartbeat_timeout
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_LAUNCHING"

  notification_metadata = <<EOF
{
  "foo": "bar"
}
EOF

  notification_target_arn = "arn:aws:sqs:us-east-1:444455556666:queue1*"
  role_arn                = "arn:aws:iam::123456789012:role/S3Access"
}